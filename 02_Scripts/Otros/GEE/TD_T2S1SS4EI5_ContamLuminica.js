var viirs = ee.ImageCollection("NOAA/VIIRS/DNB/ANNUAL_V22"),
    mx = ee.FeatureCollection("users/JonathanVSV/ImCollectionInfo/MX_inegi_gut"),
    viirs2 = ee.ImageCollection("NOAA/VIIRS/DNB/ANNUAL_V21"),
    geometry = 
    /* color: #d63000 */
    /* shown: false */
    /* displayProperties: [
      {
        "type": "rectangle"
      }
    ] */
    ee.Geometry.Polygon(
        [[[-118.963427734375, 32.74653237430332],
          [-118.963427734375, 14.494145091815875],
          [-86.60869140625002, 14.494145091815875],
          [-86.60869140625002, 32.74653237430332]]], null, false),
    ptocer = /* color: #98ff00 */ee.Geometry.Point([-99.89297336462036, 21.425469254669608]),
    ptodis = /* color: #0b4a8b */ee.Geometry.Point([-97.65176242712036, 20.52274189230723]),
    ptoaum = /* color: #ffc82d */ee.Geometry.Point([-99.21182102087036, 19.345365857250943]);

Map.addLayer(mx)

var ims = viirs2.filterBounds(mx)
               .select(['average', 'average_masked','cf_cvg'])
               .map(function(image){
                 var year = ee.Number.parse(ee.Date(image.get('system:time_start')).format('Y'));
                 var constant = ee.Image(1).rename('constant');
                 year = ee.Image.constant(year).rename('year');
                 return image.addBands(constant.float())
                             .addBands(year.float());
               });

print('ims',ims);
Map.addLayer(ims, {}, 'imagenes viirs');

var reg = ims.select(['constant', 'year', 'average'])
             .reduce(ee.Reducer.linearRegression({
               numX: 2,
               numY:1
             }));

print('reg', reg);

// The results are array images that must be flattened for display.
// These lists label the information along each axis of the arrays.
var bandNames = [['constant', 'year'], // 0-axis variation.
                 ['average']]; // 1-axis variation.

// Flatten the array images to get multi-band images according to the labels.
var lrImage = reg.select(['coefficients']).arrayFlatten(bandNames)
                 //.clip(mx)
                 // Pasar a integers para aligerarla
                 .multiply(10000)
                 .round()
                 .unmask(-999999)
                 .int32();
                 // Mask image
                 //.updateMask(ee.Image.constant(1).clip(mx));
//var rlrImage = robustLinearRegression.select(['coefficients']).arrayFlatten(bandNames);

print('lrImage',lrImage);

// Display the OLS results.
Map.addLayer(lrImage.divide(10000),{
  min: -1, max: 1,
  palette: ['#ff0000','#ffffff', '#3de738'],
  bands: ['year_average']}, 
    'OLS');
    
// Gráficos
var cero = ui.Chart.image.regions({
  image: lrImage.select('year_average'), 
  regions:[ptocer, ptodis, ptoaum], 
  reducer: ee.Reducer.first(), 
  scale: 500, 
});

print(cero)
    
Export.image.toDrive({
  image: lrImage,
  description: 'LucesNoct_RL',
  folder: 'CONABIO',
  region: geometry,
  scale: 464,
  crs: 'EPSG:6372',
  maxPixels: 1e12
});