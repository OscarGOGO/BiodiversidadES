# 17/06/2024
# Jonathan Solórzano Villegas

# Tema 3: Avances hacia la sustentabilidad
# Subtema 4: Desarrollo de capacidades

# Responsables
# EI1 Posgrados en sustentabilidad: Jonathan

#System ----
invisible(Sys.setlocale("LC_TIME","Spanish"))
options(warn = -1)

# Paqueterias ----
## Instalar y Cargar paquetes necesarios
list.of.packages <- c("stars", "sf", "raster", "terra", "rmapshaper", "purrr",
                      "progress", "magrittr", "ghql", "dplyr", "leaflet", "htmlwidgets",
                      "parallel", "jsonlite", "future", "furrr", "stringi", "httr",
                      "ows4R", "cowplot", "leaflegend")

new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if(length(new.packages) > 0){install.packages(new.packages, dependencies = TRUE)}
invisible(lapply(list.of.packages, function(x){
  suppressPackageStartupMessages(library(x, character.only = TRUE,
                                         warn.conflicts = FALSE,  quietly = TRUE))}))

# Directorios ----
tempsnib <- paste0(datadir, "03_Avances_hacia_sustentabilidad/04_Desarrollo_capacidades/05_Temporales/")
foldsecc <- paste0(datadir, "03_Avances_hacia_sustentabilidad/04_Desarrollo_capacidades/")
salida <- paste0(salidasdir, "03_Salidas/01_Tablero_datos/03_Avances_hacia_sustentabilidad/04_Desarrollo_capacidades/")

# Funciones ----
'%!in%' <- function(x,y)!('%in%'(x,y))

# Parallel ----
if(.Platform$OS.type == "unix") {
  strat <- future::multicore
} else {
  strat <- future::multisession
}
options(future.globals.maxSize= 891289600) #850mb limit
  
# Indicadores o Elementos ----
## T3S4SS0EI1 ---- 
if(2 %in% seleccion_indicador_1 | 1 %in% seleccion_indicador_1){
  # 1. Leer archivos
  cat("Corriendo EI: T3S1SS0EI1; Script: 03_Avances_sustentabilidad/01_Instrumentos.R")
  
  # Entradas
  # Actualización
  # 1. df. Anexo 1 de artículo Piña-Romero et al., 2020. Graduate Education on Climate Change and Sustainable Development. Mexico, quo vadis?
  
  df <- tryCatch(read.csv(paste0(datadir, "03_Tabla/PosgradosMx/PosgradosMx.csv"), encoding = "UTF-8"),
                    error = function(err) err)  
  if(inherits(df, "error")){
    stop("Entrada no encontrada: PosgradosMx.csv")
  }
  
  # Sacar tabla de pdf
  df <- df |>
    dplyr::mutate(dplyr::across(dplyr::everything(), ~stringr::str_trim(.x))) 
  
  resul_exp <- df |>
    dplyr::group_by(`Mexican.state`) |>
    dplyr::count()|>
    dplyr::arrange(dplyr::desc(n)) |>
    dplyr::ungroup() |>
    dplyr::rename("Estado" = "Mexican.state")
  
  json_echart$bar_chart(resul_exp,
                              x_val = "Estado",
                              clase = NULL,
                              y_val = "n",
                              y_val2 = NULL,
                              salida = paste0(salida, "04_Json/", "TD_T3S4SS0EI1_R1.json"),
                              mostrarjson = TRUE)
  
  resul_exp <- df |>
    dplyr::group_by(`Academic.institution`) |>
    dplyr::count() |>
    dplyr::arrange(dplyr::desc(n))|>
    dplyr::ungroup() |>
    dplyr::rename("Institución" = "Academic.institution")
  
  json_echart$bar_chart(resul_exp,
                               x_val = "Institución",
                               clase = NULL,
                               y_val = "n",
                               y_val2 = NULL,
                               salida = paste0(salida, "04_Json/", "TD_T3S4SS0EI1_R2.json"),
                               mostrarjson = TRUE)
  
  rm()
  invisible(gc())
  cat("\nHecho!")
}

