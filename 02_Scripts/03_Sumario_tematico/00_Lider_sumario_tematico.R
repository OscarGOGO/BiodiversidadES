#rm(list = ls())

#TABLERO DE DATOS ----
# ----Paqueterias ----
## Instalar (solo si no los encuentra en tus paquetes) y cargar paquetes necesarios
list.of.packages <-  c("biscale", "cowplot", "dplyr", "fossil", "furrr", "future", 
                       "ggplot2", "ghql", "htmlwidgets", "httr",  "iNEXT", "jsonlite", "keyring","leaflegend", "leaflet", "magrittr", "ows4R", "parallel",
                       "progress", "purrr", "raster", "RColorBrewer", "readxl" , "rlang" ,"rmapshaper", "RMariaDB",
                       "rstudioapi", "sf", "stars", "stringi", "terra")

new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if(length(new.packages) > 0){install.packages(new.packages, dependencies = TRUE)}
invisible(lapply(list.of.packages, function(x){
  suppressPackageStartupMessages(library(x, 
                                         character.only = TRUE,
                                         warn.conflicts = FALSE,  
                                         quietly = TRUE))}))
rm(list.of.packages, new.packages)

## Actualización de archivos (descargar a L)-----
cat("¿Deseas actualizar indicadores?")
actualizar <- FALSE
# Versión de datos de entrada a usar
# Aquí puedes correr una vez, actualizar = TRUE con "v2" para actualizar todos los archivos de entrada y luego correr actualizar = FALSE con "v2" para no tener que descargar los archivos de nuevo.
vers <- "v1"
# No dejar actualizar v1
vers <- ifelse(vers == "v1" & actualizar, "v2", "v1")

## Rutas de directorios ----
## Repositorio
gitlabdir <- paste0(getwd(), "/")
salidasdir <- "L:/BiodiversidadES_salidas/"
  
## Carpeta con insumos con versión de datos incluida
datadir <- paste0("L:/BiodiversidadES/", vers, "/00_Entradas/")

if(!dir.exists(datadir)){
  dir.create(datadir)
}
# Pensar en el mecanismo para poder actualizar algunas capas. Dos opciones: 
#  1) poner todas las capas como variables
#  2) descargar todas las imágenes en una carpeta y susituir si se quiere actualizar.

# Keyring para BD CAT y SNIB----
## Keyring para acceder a base de datos de CAT y SNIB
# Para crear tu keyring:
# keyring::key_set_with_value(service = "conabioMaria", username = "usuario", password = "contraseña")

# Para obtener nombre del servicio y nombre de usuario
# Verificar que corresponda al primer keyring, si no, cambiar el número dentro de los corchetes
servicio <- keyring::key_list()$service[1]
usuario <- keyring::key_list()$username[1]

# Sources ----
load(paste0(gitlabdir, "/01_Entradas/01_Funciones/CAT_SNIB_env.RData"))
load(paste0(gitlabdir, "/01_Entradas/01_Funciones/json_amchart_env.RData"))
load(paste0(gitlabdir, "/01_Entradas/01_Funciones/Diversidad_env.RData"))
load(paste0(gitlabdir, "/01_Entradas/01_Funciones/WMS_CONABIO_env.RData"))
# Lo agregué para reciclar muchas líneas del diseño de leaflet
load(paste0(gitlabdir, "/01_Entradas/01_Funciones/Leaflet_theme_env.RData"))

# Importar pipe de magrittr
import::from(magrittr, "%>%")
import::from(rlang, "!!")

# ----Sistema de referencia de coordenadas----
Cgeograficas <- 4326
Cproyectadas <- 6372

## Actualización CAT SNIB----
# Directorios actualización
# Hacer carpetas para meter archivos de actualización, si actualizar = TRUE
if(actualizar){
  t1dir <- datadir
  if(!dir.exists(t1dir)){
    dir.create(t1dir)
  }
  subdirs <- c("01_Vectorial", "02_Raster", "03_Tabla", "04_Funciones", "05_Temporales")
  purrr::walk(subdirs, function(x){
    subdir <- paste0(t1dir, x)
    if(!dir.exists(subdir)){
      dir.create(subdir)
    }
  })
}

# Actualizar CAT, SNIB, spp reportadas en la literatura
if(actualizar){
  # CAT
  ## Conectarse a la base de datos del catálogo
  con <- DBI::dbConnect(drv = RMariaDB::MariaDB(),
                        dbname = "catalogocentralizado",
                        host = "172.16.1.81",
                        username = usuario,
                        password = keyring::key_get(service = servicio, 
                                                    username = usuario))
  
  ## Sacar nombres de tablas
  nombre <- RMariaDB::dbListTables(con)
  ## Sacar los nombres de las columnas
  ## RMariaDB::dbListFields(con, nombre)
  
  df1 <- RMariaDB::dbGetQuery(con, paste0('SELECT * FROM ', nombre))
  
  subdir <- paste0(datadir, "03_Tabla/CAT")
  if(!dir.exists(subdir)){
    dir.create(subdir, 
               recursive = TRUE)
  }
  
  saveRDS(df1, paste0(subdir, "/CAT.rds"))
  rm(df1)
  gc() |> invisible()
  
  # SNIB
  # Conectarse a la base de datos del SNIB
  con <- DBI::dbConnect(drv = RMariaDB::MariaDB(),
                        dbname = "snib", #snib
                        host = "172.16.1.81",
                        username = usuario,
                        password = keyring::key_get(service = servicio, 
                                                    username = usuario))
  
  # Sacar nombres de tablas
  nombre <- RMariaDB::dbListTables(con)
  # Ver nombres de columnas
  # RMariaDB::dbListFields(con, nombre)
  
  # Para ver ejemplos
  df2 <- RMariaDB::dbGetQuery(con, paste0('SELECT idnombrecatvalido, latitud, longitud, claveestadomapa, mt24claveestadomapa, grupobio, reinovalido, phylumdivisionvalido, clasevalida, ordenvalido, familiavalida, especievalidabusqueda, zonamapa, exoticainvasora, nivelprioridad, nom059, cites, iucn, fechacolecta FROM ', nombre,' WHERE paismapa = "MEXICO" AND estatustax IN ("válido", "aceptado") AND ejemplarfosil != "SI" AND categoriavalidocatscat = "especie"'))
  
  subdir <- paste0(datadir, "03_Tabla/SNIB")
  if(!dir.exists(subdir)){
    dir.create(subdir, 
               recursive = TRUE)
  }
  saveRDS(df2, paste0(subdir, "/SNIB_mx.rds"))
  rm(df2)
  gc() |> invisible()
  
  # Especies reportadas en la literatura
  # Hacer consulta de la API de especies en la literatura
  req <- request("https://www.snib.mx/slim/src/public/v1/")
  resp <- req |> 
    # Then we add on the images path
    req_url_path_append("EspeciesLiteratura") |> 
    req_perform()
  
  resulBiodiv <- resp |>
    resp_body_json(check_type = FALSE, 
                   simplifyVector = TRUE) |> 
    purrr::pluck("resultados") |>
    as.data.frame()
  
  # Sppliteratura
  subdir <- paste0(datadir, "03_Tabla/SppLiteratura")
  if(!dir.exists(subdir)){
    dir.create(subdir, 
               recursive = TRUE)
  }
  saveRDS(resulBiodiv, paste0(subdir, "/ConteoSppTaxLit.rds"))
  rm(resulBiodiv)
  gc() |> 
    invisible()
}

# ----Procesar indicadores ----
## Favor de leer las instrucciones

## CONOCIMIENTO ----
### Diversidad biologica----
# Recordar actualizar estos números con los de 01_Diversidad_biologica
cat("Asigna una o varias de las siguientes opciones al objeto 'seleccion_indicador_1':\n
1. Todos 
2. T1S1SS0EI4")

#Ejemplo si quiero correr T1S1SS0EI1 entonces: seleccion_indicador_1  <- 2

seleccion_indicador_1 <- 1

## Año minimo y maximo para los registros del SNIB del EI: T1S2SS0EI7
## condicionales usadas igual o mayor/menor a ( >= y <=) anio_min/anio_max
anio_min = 1950
anio_max = NULL

jobRunScript(paste0(gitlabdir, 
                    "/02_Scripts/03_Sumario_tematico/01_Conocimiento/",
                    "01_Diversidad_biologica.R"), importEnv = TRUE)


# ### Estado de conocimiento----
# cat("Asigna una o varias de las siguientes opciones al objeto 'seleccion_indicador_2':\n
# 1. Todos 
# 2. T1S2SS0EI1 
# 3. T1S2SS0EI2
# 4. T1S2SS0EI3 # FALTA
# 5. T1S2SS0EI4
# 6. T1S2SS0EI5
# 7. T1S2SS0EI6")
# 
# seleccion_indicador_2 <- 1
# 
# ## Año minimo y maximo para los registros del SNIB del EI: T1S2SS0EI1
# ## condicionales usadas igual o mayor/menor a ( >= y <=) anio_min/anio_max
# anio_min_1 = NULL
# anio_max_1 = NULL
# 
# ## Año base para los registros del SNIB del EI: T1S2SS0EI2
# ## Es una suma acumulada de registros
# anio_base = 1753
# periodo <- 5 #Intervalo en años
# 
# ## Año minimo y maximo para los registros del SNIB del EI: T1S2SS0EI4
# anio_min_2 = NULL
# anio_max_2 = NULL
# 
# jobRunScript(paste0(gitlabdir, 
#                     "/02_Scripts/01_Tablero_datos/01_Conocimiento/",
#                     "02_Estado_conocimiento.R"), importEnv = TRUE)
# 
# 
# ### Desarrollo de capacidades para el conocimiento----
# 
# 
# ## CAMBIO GLOBAL----
# ### Factores de presión y amenaza----
# cat("Asigna una o varias de las opciones al objeto 'seleccion_indicador_1' de la linea siguiente:\n
# 1. Todos 
# 2. T2S1SS1EI1
# 3. T2S1SS4EI1
# 4. T2S1SS4EI2
# 5. T2S1SS5EI1")
# 
# seleccion_indicador_1 <- 1
# 
# jobRunScript(paste0(gitlabdir, 
#                     "/02_Scripts/01_Tablero_datos/02_Cambio_global/",
#                     "01_Factores_presion_amenaza.R"), 
#              encoding = "UTF-8", 
#              importEnv = TRUE)
# 
# ### Estado de conservación y tendencias de cambio----
# cat("Asigna una o varias de las opciones al objeto 'seleccion_indicador' de la linea siguiente:\n
# 1. Todos 
# 2. T2S2SS0EI1
# 3. T2S2SS0EI2
# 4. T2S2SS0EI3
# 5. T2S2SS0EI4,
# 6. T2S2SS0EI6")
# 
# seleccion_indicador <- 1
# 
# jobRunScript(paste0(gitlabdir, 
#                     "/02_Scripts/01_Tablero_datos/02_Cambio_global/",
#                     "02_Estado_conservacion_tendencias_cambio.R"), 
#              encoding = "UTF-8", 
#              importEnv = TRUE)
# 
# ### Impactos sobre la biodiversidad----
# ### Consecuencias de la pérdida de biodiversidad----
# 
# ## AVANCES HACIA LA SUSTENTABILIDAD ----
# ### Instrumentos----
# ### Programas----
# ### Acciones ----
# ### Marco Legal ----
# ### Desarrollo de capacidades para la sustentabilidad ----
# 
# ## ESCENARIOS ----
# ### Narrativas ----
# ### Causas próximas ----
# ### Impactos en biodiversidad y funciones de los ecosistemas----
# ### Contribuciones de la naturaleza a las personas ----
