# BiodiversidadES

Código de R para la plataforma BiodiversidadES. Este sitio aún se encuentra en desarrollo...

## Estructura del repositorio

El repositorio cuenta con tres ramas: 

1. main. Rama que contiene el repositorio en el estado que lo dejó Óscar ( - 07/2023).
2. develop. Rama que contiene el repositorio con las modificaciones de Jonathan (08/2023 - ).
3. ayuda. Rama que contiene los archivos de la documentación del repositorio.

## Documentación del repositorio remoto (gitlab) y local (datadir).

La estructura del repositorio donde se almacenan todos los códigos (gitlab) y donde se almacenan todas las entradas (datadir) se puede consultar en la siguiente página: [Documentación BiodiversdidadES](https://oscargogo.gitlab.io/BiodiversidadES/). Además, en dicha página vienen algunas ayudas sobre cómo instalar git, asociar gitlab a una cuenta, hacer el manejo de los repositorios locales y remotos, actualizar información del SNIB, CAT y especies reportadas en la literatura.

## Flujo de trabajo

El flujo de trabajo acordado fue el siguiente: 

**En L: (datadir)**

1. Poner scripts listos para ser integrados en Gitlab en L:.
2. Usar git (local) en BiodiversidadES - L: para seguir cambios y modificaciones de los scripts.
3. A partir de noviembre 2024, se dio acceso de escritura a más personas para que suban directamente el código del json al repositorio.

**En Gitlab (gitlabdir)**

3. Usar la sección de [Issues](https://gitlab.com/OscarGOGO/BiodiversidadES/-/issues) en Gitlab para llevar un control de los scripts a desarrollar, integrar y revisar.
4. Ir cambiando las etiquetas conforme se avance en el trabajo.

## Acuerdos: 

1. Hacer un _commit_ por EI cuando quede revisado e integrado.
2. Al integrar el script no hacer cambios de fondo, sino solo de forma (u optimización). En caso de requerir cambios de fondo, hacerlo en un segundo commit para facilitar el seguimiento por el resto del equipo.
3. En los scripts agregar comentarios que permitan entender los pasos de los procesamientos.

## Lista de indicadores y status

Para consultar el estado del avance en la integración de los scripts de los indicadores, consultar la sección de [Issues](https://gitlab.com/OscarGOGO/BiodiversidadES/-/issues).

## Guía de commits

Plantilla de commit sugerido:

```md
Título de commit  

Describir en tiempo presente los cambios realizados brevemente. Si el commit resuelve algún _issue_ en particular, se puede hacer referencia al número de 
issue con "#1" o también usando el hash de referencia "7as7b101", o cerrarlo usando Fix o Close como "Fixes #2" o "Closes #3".

Changelog: changed
```

Entradas válidas para el changelog:

- added: New feature
- fixed: Bug fix
- changed: Feature change
- deprecated: New deprecation
- removed: Feature removal
- security: Security fix
- performance: Performance improvement
- other: Other

## Revisar historia de un _script_ en particular

1. Irse a la sección del repositorio [Repository](https://gitlab.com/OscarGOGO/BiodiversidadES/-/tree/main).
2. Navegar al script de interés y darle clic hasta ver el contenido del código.
3. En la parte superior derecha del script darle clic al botón de **Blame**.
4. Revisar qué commit agregó qué líneas en la versión final.
5. Si se desea consultar la versión anterior (i.e., de un commit anterior), dar clic en el botón de tres documentos entre el nombre del commit y el número de línea del código.

## Actualización de los indicadores (Acuerdos 06/06/24)

Todas las entradas que usan los EI están contenidas en "datadir" donde se indica la versión de estas. Las primeras entradas de trabajo (actuales) son las de "v1", mientras que las entradas para actualizar (futuras) se esperaría que estuvieran en "v2". El código de cada indicador primero revisa si existen las entradas en la versión de interés ("v1" o "v2"), si no existe, en comentarios viene la información de dónde se descargó cada información. Las entradas de "v1" se pueden usar como plantillas para saber en qué formato se tienen que acomodar los datos de "v2". Finalmente, los datos que se pueden actualizar de manera automática (CAT, SNIB y Especies reportadas en la literatura), se pueden descargar las nuevas versiones poniendo actualizar = TRUE en los scripts líder. 

# Generación de Changelog

Si se desea hacer un registro de todos los commits y cambios realizados, se puede hacer una versión rápida con el siguiente código en git (desde la terminal de Rstudio).

`git log --pretty='format:-%cd %s' --date=format:'%Y-%m-%d' > CHANGELOG.md`