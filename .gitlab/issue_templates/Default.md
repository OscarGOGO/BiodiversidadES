## Resumen del issue

Breve resumen del issue

## Énfasis en:

Qué aspectos del issue o del EI en cuestión son relevantes de destacar en esta versión (cambio, ajustes, etc).

## Estado actual (opcional)

Cómo está la versión actual del EI

## Estado deseado (opcional)

Cómo debería quedar el EI
