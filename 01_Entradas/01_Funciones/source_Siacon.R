Siacon <- new.env()

#' Realizar pre-procesamiento salidas de la app de SIACON
#' 
#' @param df data.frame. Data frame importado en R del txt de SIACON.
#' @param equival data.frame. Data frame con dos columnas: 1) clase: el nombre de los bloques o variables incluidas en la exportación del sistema de siacon y 2) el número de NA consecutivo que equivalen a dicha clase. Se usa la posición de las columnas para realizar el procesamiento, así que el nombre no importa, pero sí la posición de las columnas.
#' @param colInfo character. Nombre de la columna que contiene la información a extraer de los bloques. Típicamente sería la columna "X."
#' @param colNum character. Nombre de la columna que contiene la información numérica para hacer el conteo de NA. Típicamente puede ser la primera columna con valores numéricos, p.ej., "sembrada".
#' @returns a data.frame.
#' @examples
#'  Siacon$conv2table(df = df,
#'                    equival = data.frame(clase = c("anio", 
#'                                                   "estado",
#'                                                   "municipio",
#'                                                   "modalidad"),
#'                                         numNa = seq(1,4,1)),
#'                    
#'                    colInfo = "X.",
#'                    colNum = "sembrada")
#'

Siacon$conv2table <- function(df, equival, colInfo, colNum){
  
  df <- df |>
    # Quitar NA en colInfo e ignorar columnas lógicas
    tidyr::drop_na(!!rlang::sym(colInfo)) |>
    dplyr::select(!where(is.logical)) |>
    # Llenar espacios en blanco como NA si no se hace al leer data.table
    dplyr::mutate(dplyr::across(-!!rlang::sym(colInfo), ~ gsub("[\\,A-z]", "", .x))) |>
    dplyr::mutate(dplyr::across(!where(is.logical), ~ dplyr::na_if(.x,""))) |>
    dplyr::mutate(dplyr::across(-!!rlang::sym(colInfo), ~ as.numeric(.x))) |>
    # Lo de !!rlang::sym es para poder usar la cadena de texto como nombre sin comillas
    # como se mencionana las columnas en el tidyverse
    dplyr::mutate(isNA = ifelse(is.na(!!rlang::sym(colNum)), 1, 0),
                  nNa = cumsum(isNA),
                  groups = ifelse(isNA == dplyr::lead(isNA), "NoChange", "Change"),
                  tracker = 0,
                  n = dplyr::row_number(),
                  isNum = ifelse(!is.na(!!rlang::sym(colNum)), 1, 0),
                  nNum = cumsum(isNum))
  
  # Restarle 1 a los nNum para luego usarlos para sumarlos al start
  df$nNum[df$isNA == 0] <- df$nNum[df$isNA == 0]-1 
  
  # Número máx de categorías
  max_cats <- nrow(equival)
  
  # Crear df de las filas donde hay NAs y número de NAs consecutivos
  cnums <- c(0, which((df$groups == "Change" & df$isNA == 0)|(df$groups == "NoChange" & df$isNA == 0)), nrow(df))
  temp <- tibble::tibble(cnums = cnums,
                         indices = cnums - dplyr::lag(cnums)- 1,
                         fill = dplyr::lead(indices),
                         index2 = cnums+1) 
  temp <- temp[which(temp$fill >= 0 & !is.na(temp$fill)),]
  
  # Agregar contador de NAs continuos a df y crear números de clave única para cada nivel de NA para luego poder hacer el pivot_wider y fill
  df <- df |>
    dplyr::group_by(nNum) |>
    dplyr::mutate(counter = dplyr::row_number()) |>
    dplyr::ungroup() |>
    dplyr::left_join(temp |>
                       dplyr::select(index2, fill),
                     by = c("n" = "index2")) |>
    dplyr::mutate(tracker3 = dplyr::case_when (
      isNA == 0 ~ 0,
      TRUE ~ NA
    )) |>
    dplyr::mutate(tracker4 = dplyr::case_when(
      is.na(tracker3) ~ fill,
      tracker3 == 0 ~ 0,
      TRUE ~ NA
    )) |>
    tidyr::fill(tracker4, 
                .direction = "downup") |>
    dplyr::mutate(start = ifelse(tracker4 == 0, NA, max_cats - tracker4+1),
                  end = ifelse(tracker4 == 0, NA, start + tracker4-1)) |>
    dplyr::mutate(tracker3 = start + counter -1) 
  
  # Crear columnas nuevas de acuerdo con la tabla equival
  extra_cols <- suppressMessages(1:nrow(equival) |>
    purrr::map(~ ifelse(df$tracker3 == equival[.x,2], df |>
                          dplyr::pull(!!rlang::sym(colInfo)), NA)) |>
    dplyr::bind_cols() |>
    # renombrar
    `colnames<-`(equival |>
                   dplyr::pull(1)) |>
    tidyr::fill(everything(),
                .direction = "downup"))
  
  # Unir columnas con df original y sacar las entradas NA en colInfo, así como seleccionar solo las columnas originales
  df <- dplyr::bind_cols(extra_cols, df) |>
    dplyr::filter(is.na(tracker3)) |>
    dplyr::filter(!!rlang::sym(colInfo) != "TOTAL" & !!rlang::sym(colInfo) != "Total" & !!rlang::sym(colInfo) != "total") |>
    dplyr::select(-c(isNA, nNa, groups, tracker, n, isNum, nNum, counter, fill, tracker3, tracker4, start, end))
  
  # Regresar df
  return(df)
}

# save(Siacon, file = paste0(gitlabdir, "/01_Entradas/01_Funciones/Siacon_env.RData"))

