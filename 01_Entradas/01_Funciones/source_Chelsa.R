# Funciones para descarga y porcesamiento de datos Chelsa

# Función de descarga----
# Función para descargar imágenes de Chelsa de los parámetros y años de interés
# Esto es para bajar los archivos de Timeseries
dwnld_chelsa <- function(combs, url_base, ruta){
  # Descargar todas las imágenes (tarda bastante)
  # 1. Revisar https://envicloud.wsl.ch/#/?prefix=chelsa%2Fchelsa_V2%2FGLOBAL%2F, ver los años disponibles y asegurarse que los años usados en el segundo ciclo estén
  
  # https://os.zhdk.cloud.switch.ch/envicloud/chelsa/chelsa_V2/GLOBAL/monthly/tas/CHELSA_tas_01_1980_V.2.1.tif
  urls <- paste0(url_base,combs[,1],"/CHELSA_",combs[,1],"_",combs[,2],"_",combs[,3],"_V.2.1.tif")
  files <- paste0(ruta, "/CHELSA_",combs[,1],"_",combs[,2],"_",combs[,3],"_V.2.1.tif")
  
  # Ordenar precipitación y tas; pr son más pesados que tas
  # Si no se ordenan tas termina y se queda esperando a pr
  urls <- urls[order(urls)]
  files <- files[order(files)]
  
  # Descargar todas las imágenes en grupos de 6, guardar respuesta en resp para analizar cuáles no se descargaron: resp[[1]]$status_code == 200, bien descargadas.
  resp <- purrr::map(seq(1, length(urls), 6),
                     function(x){
                       fin <- ifelse((x+5)<=length(urls), x+5, length(urls))
                       cat("Descargando: ", x)
                       curl::multi_download(urls = urls[x:fin],
                                            destfiles = files[x:fin],
                                            progress = TRUE)
                     })
  return(resp)
}

# Función de corte----
# Función para cortar imagen a México y sobreescribir las imágenes anteriores, para ahorrar espacio en disco
cropper <- function(Mexico, imgs){
  
  # Sacar bbox de MX
  bbox <- Mexico |>
    sf::st_transform(Cgeograficas) |>
    sf::st_bbox() |>
    sf::st_as_sfc() |>
    sf::st_as_sf() |>
    dplyr::mutate(pais = "MX")
  
  # Cortar imágenes a MX y sobreescribir en ruta original
  purrr::walk(imgs, function(x){
    ftmp <- paste0(tempfile(), ".tif")
    file.rename(x, ftmp)
    im <- tryCatch(terra::rast(ftmp),
                   error = function(err) err)
    if(!inherits(im, "error")){
      im <- crop(im, vect(bbox))
      writeRaster(im, x, overwrite=TRUE)
      file.remove(ftmp)
    }else{
      cat("\n No hay raster: ", x)
    }
    
  })
}

# Función cálculo de variables de referencia----
# Cálculo de las condiciones promedio en 30 años para usarlo como periodo de referencia
prom_30yrs <- function(imgs, vars, months, proc_folder){
  purrr::walk(vars,
              function(var){
                furrr::future_walk(months,
                                   function(month){
                                     # Sacar archivos de cada variables y mes de 1980-2010
                                     month <- stringr::str_pad(month, 2, "left", "0")
                                     subimgs <- imgs[stringr::str_detect(imgs, paste0(var, "_",month,"_"))]
                                     # Sacar imágenes de 1980 - 2009
                                     subimgs <- subimgs[stringr::str_detect(subimgs, "\\d{2}_([1-2][0-9](([8-9][0-9])|([0][0-9])))")]
                                     
                                     ims <- purrr::map(subimgs, terra::rast)
                                     # pasar de lista a multibanda
                                     ims <- terra::rast(ims)
                                     # Promedio por mes
                                     ims <- terra::mean(ims, na.rm = TRUE)
                                     # Escalar por unidad (ver metadatos de Chelsa)
                                     if(var == "pr"){
                                       ims <- ims * 0.1
                                     }
                                     if(var == "tas"){
                                       ims <- (ims * 0.1)-273.15
                                     }
                                     
                                     writeRaster(ims,
                                                 paste0(proc_folder,"/CHELSA_",var,"_",month,"_1980-2009_V.2.1.tif"),
                                                 overwrite = TRUE)
                                   },
                                   .options = furrr_options(packages = c("terra", "purrr")))
              })
}

# Función de anomalías----
# Cálculo de anomalías como la diferencia entre cada imagen contra la del periodo de referencia.
anoms_func <- function(imgs, imgs_proc, vars){
  anoms <- purrr::map(vars,
                      function(var){
                        # Ciclo mes
                        resul <- purrr::map(1:12,
                                            function(month){
                                              # Sacar archivos de cada variables y mes de 1980-2010
                                              month <- stringr::str_pad(month, 2, "left", "0")
                                              subimgs <- imgs[stringr::str_detect(imgs, paste0(var, "_",month,"_"))]
                                              subimgs_proc <- imgs_proc[stringr::str_detect(imgs_proc, paste0(var, "_",month,"_"))]
                                              meanim <- terra::rast(subimgs_proc)
                                              ims <- purrr::map(subimgs, 
                                                                function(x){
                                                                  im <- terra::rast(x)
                                                                  # Escalar por unidad (ver metadatos de Chelsa)
                                                                  if(var == "pr"){
                                                                    im <- im * 0.1
                                                                  }
                                                                  if(var == "tas"){
                                                                    im <- (im * 0.1)-273.15
                                                                  }
                                                                  
                                                                  resul <- im-meanim
                                                                  return(resul)
                                                                })
                                              # pasar de lista a multibanda
                                              ims <- terra::rast(ims)
                                              # Promedio por mes
                                              ims <- terra::mean(ims, 
                                                                 na.rm = TRUE)
                                              return(ims)
                                            })
                        return(resul)
                      }) 
  
  anoms <- purrr::list_flatten(anoms)
  return(anoms)
}
