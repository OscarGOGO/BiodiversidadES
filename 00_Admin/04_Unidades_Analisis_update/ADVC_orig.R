library(tidyverse)
library(sf)
library(readr)
library(stringr)
library(stringi)

datadir <- "L:/BiodiversidadES/v1/00_Entradas"

# Crear carpeta temporal
td <- tempdir()

# Crear archivo temporal
tf <- tempfile(tmpdir=td, 
               fileext=".zip")

# Descargar archivo desde url
download.file("http://sig.conanp.gob.mx/website/interactivo/advc/descargas/ADVC.zip",
              tf)

# Enlistar archivos para extraer
file_names <- unzip(tf, list=TRUE)

# Extraer archivos
unzip(tf, exdir=td, overwrite=TRUE)

# Buscar el shp y leerlo
advc_new <- st_read(paste0(datadir, "/00_Unidades_analisis/update_20241122/602_ADVC_SEPTIEMBRE_2024.shp")) |>
  st_make_valid() |>
  st_transform(6372)

advc_old <- st_read(list.files(paste0(datadir, "/00_Unidades_analisis/backup_20241122/"),
                                  "*ADVC_proj.gpkg",
                                  full.names = TRUE)) 

corr_names <- c("Area" =  "Área",
  "CostaSalvaje" = "Costa Salvaje",
  "WildLands" = "Wildlands",
  "otate" = "Otate",
  "EL" = "El",
  "Biol." = "Biól.")

# Opción 1-----
# Si falla el st_make_valid, quitar esférico
resul <- advc_new |>
  st_make_valid() |>
  group_by(ADVC, ESTADO) |>
  summarise(ID = paste0(ID, collapse = ";"),
            MUNICIPIO = paste0(MUNICIPIO, collapse = ";"),
            TIPO_PROP = paste0(TIPO_PROP, collapse = ";"),
            NO_CERT = paste0(NO_CERT, collapse = ";"),
            PRIM_DEC = paste0(FECHA_EXP, collapse = ";"),
            .groups = "drop") |>
  mutate(across(ADVC, ~ str_replace_all(.x, corr_names))) |>
  arrange(ID) |>
  mutate(ID_POLI = row_number()) |>
  rename("NOM" = "ADVC",
         "ID_Orig" = "ID") |>
  dplyr::select(ID_POLI, NOM, ESTADO, MUNICIPIO, ID_Orig, everything(), geometry) |>
  st_cast("MULTIPOLYGON") |>
  st_set_geometry("geom") 

# Comentado la escritura para no sobreescribir por error
resul |>
  st_transform(6372)# |>
  # st_write(paste0(datadir, "/00_Unidades_analisis/3_ADVC_proj.gpkg"),
  #          append = FALSE)

resul |>
  st_transform(4326) #|>
  # st_write(paste0(datadir, "/00_Unidades_analisis/3_ADVC_wgs.gpkg"),
  #          append = FALSE)

resul |>
  st_drop_geometry() |>
  dplyr::select(ID_POLI, NOM, ID_Orig, ESTADO, MUNICIPIO) |>
  write.csv("C:/Users/jsolorzano/Documents/R/temp/ID_ADVC.csv",
            row.names = F)

# Opción 2-----