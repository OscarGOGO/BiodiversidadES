library(sf)
library(tidyverse)

muns <- st_read(paste0(datadir, "00_Unidades_analisis/9_Municipios_wgs.gpkg"))
df <- read.csv(paste0("L:/BiodiversidadES_salidas/03_Salidas/01_Tablero_datos/02_Cambio_global/01_Factores_presion_amenaza/01_Tablas/TD_T2S1SS3EI2_R1.csv"),
               encoding = "UTF-8")

resul <- muns |>
  mutate(across(ID_POLI, ~as.numeric(.x))) |>
  left_join(df, 
            by = "ID_POLI")

st_write(resul, 
         paste0("C:/Users/jsolorzano/Documents/R/temp", "/muns_prueba.gpkg"),
         append = FALSE)

json_echart$aux_gpkg(capas = "muns_prueba.gpkg",
                     estilo = "Polígonos",
                     campos = list(colnames(df)[str_detect(colnames(df), "_Volumen")]),
                     etiquetas = list(c("NOM", "NOMGEO", colnames(df)[str_detect(colnames(df), "_Volumen")], colnames(df)[str_detect(colnames(df), "_ValProPesosConst")])),
                     nombres = "Municipios",
                     paletas = list(paletas$paletas("viridis", 22)),
                     salida = paste0("C:/Users/jsolorzano/Documents/R/temp", "/muns_prueba_aux.json"))
